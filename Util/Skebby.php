<?php

namespace Zen\Bundle\SkebbyBundle\Util;

use Guzzle\Http\ClientInterface;

/**
 * Skebby main class
 *
 */
class Skebby
{
    const NET_ERROR             = "Errore+di+rete+impossibile+spedire+il+messaggio";
    const SENDER_ERROR          = "Puoi+specificare+solo+un+tipo+di+mittente%2C+numerico+o+alfanumerico";
    const SMS_TYPE_CLASSIC      = "classic";
    const SMS_TYPE_CLASSIC_PLUS = "classic_plus";
    const SMS_TYPE_BASIC        = "basic";

    protected
        $httpClient,
        $username,
        $password,
        $testMode;

    protected static $errors = array(
        10 => "Errore generico",
        11 => "Charset non valido",
        12 => "Qualche parametro obbligatorio non è stato specificato",
        20 => "Parametri non validi",
        21 => "Username o password non validi",
        22 => "Mittente non valido",
        23 => "Mittente troppo lungo (oltre gli 11 caratteri)",
        24 => "Testo troppo lungo",
        25 => "Destinatario non valido",
        26 => "Mittente non impostato",
        27 => "Troppi destinatari",
        29 => "Il tuo account non è configurato per usare il gateway SMS",
        30 => "Credito insufficiente per inviare il messaggio",
        31 => "Sono accettatte solo richieste HTTP con metodo POST",
        32 => "Il formato del delivery_start non è valido, usa il formato RFC 2822 es: Mon, 15 Aug 2005 15:52:01 +0000",
        33 => "L' encoding_scheme non è valido, valori accettati: normal, ucs2 visita http://en.wikipedia.org/wiki/GSM_03.38 per maggiori informazioni.",
        34 => "Il validity_period non è valido, deve essere un valore intero (espresso in minuti) più grande di 0 e minore di 2880 (2 giorni)",
        35 => "L' user_reference non è valido, deve avere una lunghezza massima di 20 caratteri [a-zA-Z0-9-_+:;]",
        36 => "Se hai impostato il delivery_start e vuoi il rapporto di consegna devi specificare obbligatoriatamente anche il campo user_reference",
        37 => "Stai inviando al gateway dei caratteri nel charset sbagliato, controlla il prametro charset",
    );

    /**
     * Constructor
     *
     * @param ClientInterface $httpClient
     * @param string          $username
     * @param string          $password
     * @param boolean         $testMode
     */
    public function __construct(ClientInterface $httpClient, $username, $password, $testMode = false)
    {
        $this->httpClient = $httpClient;
        $this->username = $username;
        $this->password = $password;
        $this->testMode = $testMode;
    }

    /**
     * Send SMS
     *
     * @param array   $recipients
     * @param string  $text
     * @param integer $sms_type
     * @param string  $sender_number
     * @param string  $sender_string
     * @param string  $user_reference
     * @param string  $charset
     * @param array   $optional_headers
     */
    public function sendSMS(array $recipients, $text, $sms_type = self::SMS_TYPE_CLASSIC, $sender_number = '', $sender_string = '', $user_reference = '', $charset = '', array $optional_headers = null)
    {
        $recipients = $this->cleanRecipients($recipients);

        switch ($sms_type) {
            case self::SMS_TYPE_CLASSIC:
                $method = 'send_sms_classic';
                break;
            case self::SMS_TYPE_CLASSIC_PLUS:
                $method = 'send_sms_classic_report';
                break;
            case self::SMS_TYPE_BASIC:
                $method = 'send_sms_basic';
                break;
            default:
                throw new \UnexpectedValueException(sprintf('Invalid SMS type: %s', $sms_type));
        }

        // TODO maybe it's better to remove $this->testMode and mock Guzzle\Client
        if ($this->testMode) {
            $method = 'test_' . $method;
        }

        $parameters = array(
            'method'     => $method,
            'text'       => $text,
            'recipients' => $recipients
        );

        if ($sender_number != '' && $sender_string != '') {
            return array(
                'status'  => 'failed',
                'message' => self::SENDER_ERROR,
            );
        }
        if ($sender_number != '') {
            $parameters['sender_number'] = $sender_number;
        }
        if ($sender_string != '') {
            $parameters['sender_string'] = $sender_string;
        }
        if ($user_reference != '') {
            $parameters['user_reference'] = $user_reference;
        }

        switch ($charset) {
            case 'UTF-8':
                $parameters['charset'] = 'UTF-8';
                break;
            case '':
            case 'ISO-8859-1':
            default:
        }

        return $this->do_post_request($parameters, $optional_headers);
    }

    /**
     * Get credit
     *
     * @param  string $charset
     * @return array
     *
     */
    public function getCredit($charset = '')
    {
        $parameters = array(
            'method' => 'get_credit',
        );

        switch ($charset) {
            case 'UTF-8':
                $parameters['charset'] = 'UTF-8';
        }

        return $this->do_post_request($parameters);
    }

    /**
     * Check if result is an error
     *
     * @param  array   $result
     * @return boolean
     */
    public function isResultError(array $result)
    {
        return $result['status'] != 'success';
    }

    /**
     * Get result error message
     *
     * @param  array  $result
     * @return string
     */
    public function getResultErrorMessage(array $result)
    {
        return isset($result['code']) ? $this->getErrorMessage($result['code']) : '';
    }

    /**
     * Get error message
     *
     * @param  string $error
     * @return string
     */
    public function getErrorMessage($error)
    {
        if (array_key_exists($error, self::$errors)) {
            return self::$errors[$error];
        }

        return "Errore sconosciuto";
    }

    /**
     * TODO maybe here we could check for prefix in each recipient?
     *
     * @param  array $recipients
     * @return array
     */
    protected function cleanRecipients(array $recipients)
    {
        return $recipients;
    }

    /**
     * Do POST request
     *
     * @param  array $data
     * @param  array $optional_headers
     * @return array
     */
    protected function do_post_request(array $data, array $optional_headers = null)
    {
        $data['username'] = $this->username;
        $data['password'] = $this->password;

        $request = $this->httpClient->post(null, $optional_headers, $data);
        $response = $request->send();

        return $this->parseQueryString($response->getBody());
    }

    /**
     * Parse query string returned by Skebby webservice
     *
     * @param  string $str
     * @return array
     */
    protected function parseQueryString($str)
    {
        $op = array();
        $pairs = explode("&", $str);
        foreach ($pairs as $pair) {
            list($k, $v) = array_map("urldecode", explode("=", $pair));
            $op[$k] = $v;
        }

        return $op;
    }
}
