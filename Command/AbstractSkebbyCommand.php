<?php

namespace Zen\Bundle\SkebbyBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

abstract class AbstractSkebbyCommand extends ContainerAwareCommand
{
    /**
     * Get Skebby service
     *
     * @return \Zen\Bundle\SkebbyBundle\Util\Skebby
     */
    protected function getSkebby()
    {
        return $this->getContainer()->get('skebby');
    }
}
