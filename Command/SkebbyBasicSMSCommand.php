<?php

namespace Zen\Bundle\SkebbyBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zen\Bundle\SkebbyBundle\Util\Skebby;

class SkebbyBasicSMSCommand extends AbstractSkebbySMSCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();
        $this
            ->setName('skebby:sms:basic')
            ->setDescription('invia un semplice sms')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $skebby = $this->getSkebby();

        $result = $skebby->sendSMS(array($input->getArgument('destinatari')), $input->getArgument('testo'), Skebby::SMS_TYPE_CLASSIC);

        $output->writeln('');
        if (!$skebby->isResultError($result)) {
            $output->writeln('<info>OK</info>');
        } else {
            $output->writeln(sprintf('Errore: <error>%s</error>', $skebby->getResultErrorMessage($result)));
        }

        $output->writeln('');
    }

}
