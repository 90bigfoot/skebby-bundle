<?php

namespace Zen\Bundle\SkebbyBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SkebbyCreditCommand extends AbstractSkebbyCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('skebby:credit')
            ->setDescription('Credito residuo skebby')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $skebby = $this->getSkebby();
        $result = $skebby->getCredit();
        $output->writeln('');
        if (!$skebby->isResultError($result)) {
            $output->writeln(sprintf('Credito residuo euro: <info>%s</info>', $result['credit_left']));
            $output->writeln(sprintf('Classic SMS: <info>%s</info>', $result['classic_sms']));
            $output->writeln(sprintf('Basic SMS: <info>%s</info>', $result['basic_sms']));
        } else {
            $output->writeln(sprintf('Errore: <error>%s</error>', $skebby->getResultErrorMessage($result)));
        }
        $output->writeln('');
    }
}
