[![Latest Stable Version](https://poser.pugx.org/zen/skebby-bundle/v/stable.png)](https://packagist.org/packages/zen/skebby-bundle)
[![Total Downloads](https://poser.pugx.org/zen/skebby-bundle/downloads.png)](https://packagist.org/packages/zen/skebby-bundle)


What is zen\skebby-bundle?
--------------------------

A bundle that give you a SMS gateway via Italian Skebby company.

Installation
------------

In ``composer.json`` use
```
require:
    "zen/skebby-bundle": "1.0.*"
```

In your ``app/AppKernerl.php`` add:
```php
new Zen\Bundle\SkebbyBundle\SkebbyBundle()
```

In your ``app/config/parameters.yml`` add:
```
skebby_username: myskebbyaccount  # customize this
skebby_password: myskebbypass     # customize this
```

In your ``app/config/config.yml`` add:
```
skebby:
    username:   %skebby_username%
    password:   %skebby_password%
    # if you want to simulate only the send,
    # uncomment next line
    # but a valid account is required
    #test_mode: true
```

Usage
-----

In your controller use:
```php
    use Zen\Bundle\SkebbyBundle\Util\Skebby;

    public function sendSmsAction()
    {
        $skebby = $this->getSkebby();

        $text = "Hello!";
        $result = $skebby->sendSMS(array('tel1', 'tel2', 'telx'), $text, Skebby::SMS_TYPE_CLASSIC);
    }

    /**
     * Get Skebby service
     *
     * @return \Zen\Bundle\SkebbyBundle\Util\Skebby
     */
    private function getSkebby()
    {
        return $this->getContainer()->get('skebby');
    }
```

You can use these console commands:

```
$ php app/console skebby:credit
$ php app/console skebby:sms:basic <number> <text message>
```
