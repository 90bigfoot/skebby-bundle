<?php

namespace Zen\Bundle\SkebbyBundle\Command;

use Symfony\Component\Console\Input\InputArgument;

abstract class AbstractSkebbySMSCommand extends AbstractSkebbyCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->addArgument('destinatari', InputArgument::REQUIRED)
            ->addArgument('testo', InputArgument::REQUIRED)
        ;
    }
}
